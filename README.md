Hi there! 👋 I'm Ilya
===========

### 🧑‍💻 About Me

My name is Ilya Rodin.  
I am Junior Frontend-developer from Penza, Russia.  
I am studying at [Hexlet](https://ru.hexlet.io/) development school.  
  
**I am currently looking for carrer opportunities as Junior Frontend-developer.**  
**My CV:** https://cv.hexlet.io/ru/resumes/2170

### 📲 Contacts

[![Gmail](https://img.shields.io/badge/Email-D14836?style=for-the-badge&logo=gmail&logoColor=white)](i1.rodin@yandex.ru)
[![Telegram](https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white)](https://t.me/eternal_struggler)

### 🛠️ Languages and Tools

<p align="left">
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/javascript-colored.svg" width="36" height="36" alt="JavaScript" /></a>
<a href="https://developer.mozilla.org/en-US/docs/Glossary/HTML5" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/html5-colored.svg" width="36" height="36" alt="HTML5" /></a>
<a href="https://www.w3.org/TR/CSS/#css" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/css3-colored.svg" width="36" height="36" alt="CSS3" /></a>
<a href="https://reactjs.org/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/react-colored.svg" width="36" height="36" alt="React" /></a>
<a href="https://getbootstrap.com/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/bootstrap-colored.svg" width="36" height="36" alt="Bootstrap" /></a>
</p>
<a href="https://jestjs.io/" target="_blank" rel="noreferrer"><img src="https://readme-components.vercel.app/api?component=logo&fill=whitesmoke&logo=jest&svgfill=C21325&text=false" height=45 alt="Webpack" /></a>
<a href="https://eslint.org/" target="_blank" rel="noreferrer"><img src="https://readme-components.vercel.app/api?component=logo&fill=whitesmoke&logo=eslint&svgfill=4c33c1&text=false" height=45 alt="Webpack" /></a>
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"><img src="https://readme-components.vercel.app/api?component=logo&fill=whitesmoke&logo=git&svgfill=f1502f&text=false" height=45 alt="Webpack" /></a>
<a href="https://github.com/" target="_blank" rel="noreferrer"><img src="https://readme-components.vercel.app/api?component=logo&fill=whitesmoke&logo=github&svgfill=black&text=false" height=45 alt="Webpack" /></a>
<a href="https://ubuntu.com/" target="_blank" rel="noreferrer"><img src="https://readme-components.vercel.app/api?component=logo&fill=whitesmoke&logo=ubuntu&svgfill=e95420&text=false" height=45 alt="Webpack" /></a>

### 💻 My Projects
[![Gendiff Card](https://github-readme-stats.vercel.app/api/pin/?username=ilya-rodin&repo=gendiff&theme=react)](https://github.com/ilya-rodin/gendiff)

[![Brain-Games Card](https://github-readme-stats.vercel.app/api/pin/?username=ilya-rodin&repo=brain-games&theme=react)](https://github.com/ilya-rodin/brain-games)


### 📉 GitHub Stats

<a href="http://www.github.com/ilya-rodin">
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=ilya-rodin&theme=react&show_icons=true&hide=&count_private=true&hide_border=true&show_icons=true" alt="artch3r's GitHub stats" />
</a>
<a href="http://www.github.com/ilya-rodin">
  <img align="center" width=350 src="https://github-readme-stats.vercel.app/api/top-langs/?username=ilya-rodin&theme=react&layout=compact&langs_count=10&hide_border=true&locale=en&custom_title=Top%20%Languages" alt="Top Languages" />
</a>
